#include "config.h"
#include <iostream>
#include <CLI/CLI.hpp>

#include "network_service_wamp_wrapper.hpp"

void show_version(size_t) {
	std::cout << PACKAGE_STRING << std::endl; 
	throw CLI::Success();
}

int main(int argc, char** argv)
{
	CLI::App app{PACKAGE_NAME};

	std::string uri   	 = "ws://127.0.0.1:9001";
	std::string realm 	 = "fabui";
	std::string	env_file = "/var/www/.env";

	app.add_option("-u,--uri", uri, "WAMP default realm [default: " + uri + "]");
	app.add_option("-r,--realm", realm, "WAMP default realm [default: " + realm + "]");
	app.add_option("-e,--backend-env", env_file, "FABUI web .env file [default: " + env_file + "]");
	app.add_config("-c,--config", "config.ini", "Read config from an ini file", false);
	app.add_flag_function("-v,--version", show_version, "Show version");

	try {
		app.parse(argc, argv);
	} catch (const CLI::ParseError &e) {
		return app.exit(e);
	}	
	
	try {
		
		fabui::NetworkService service;
		fabui::NetworkServiceWrapper wampservice(env_file, service);

		if( wampservice.connect(uri, realm)) {
			auto finished = wampservice.finished_future();
			finished.wait();
		}

	} catch (const std::exception& e) {
		std::cout << e.what() << std::endl;
		return 1;
	}
}