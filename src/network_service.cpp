#include "config.h"
#include "network_service.hpp"

#include <fabui/utils/string.hpp>
#include <fabui/utils/os.hpp>

#define FMT_HEADER_ONLY
#include <fmt/format.h>

#include <dbusxx/dbus.hpp>

using namespace fabui;
using namespace wampcc;
using namespace std::placeholders;

NetworkService::NetworkService()
	: m_session(nullptr)
{
}

NetworkService::~NetworkService() {

}

void NetworkService::setWampSession(IWampSession* session)
{
	m_session = session;
}

IWampSession* NetworkService::getWampSession()
{
	return m_session;
}