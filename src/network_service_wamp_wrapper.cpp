#include "config.h"
#include "network_service_wamp_wrapper.hpp"

#define MAX_WORKERS		8

using namespace fabui;
using namespace wampcc;
using namespace std::placeholders;

NetworkServiceWrapper::NetworkServiceWrapper(const std::string& env_file, NetworkService& service)
	: BackendService(PACKAGE_STRING, env_file, MAX_WORKERS, false)
	, m_service(service)
{
	m_service.setWampSession(this);
}

void NetworkServiceWrapper::onConnect()
{
	join( realm(), {"token"}, "network-service");
}

void NetworkServiceWrapper::onDisconnect()
{
	// m_service.stop();
}

void NetworkServiceWrapper::onJoin()
{
	std::cout << "Joined\n";
	
	provide("network.service.terminate", std::bind(&NetworkServiceWrapper::terminate_wrapper, this, _1));
	publish("network.events", { {"network_service_started"}, {} });

	std::cout << PACKAGE_NAME" started\n" << std::flush;
}

void NetworkServiceWrapper::terminate_wrapper(invocation_info info) {
	yield(info.request_id, {true});
	disconnect();
}