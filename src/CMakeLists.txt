set(SERVICE_NAME fabui-network-service)

set(SOURCES
	"${PROJECT_SOURCE_DIR}/src/main.cpp"
	"${PROJECT_SOURCE_DIR}/src/network_service.cpp"
	"${PROJECT_SOURCE_DIR}/src/network_service_wamp_wrapper.cpp"
	)

add_compile_options(-Wall -Wextra)

add_executable(${SERVICE_NAME} ${SOURCES})
set_property(TARGET ${SERVICE_NAME} PROPERTY CXX_STANDARD 14)
set_property(TARGET ${SERVICE_NAME} PROPERTY CXX_STANDARD_REQUIRED ON)

target_include_directories(${SERVICE_NAME} PUBLIC ${3RDPARTY_SOURCE_DIR})

target_link_libraries (${SERVICE_NAME} ${LIBFABUI_LIBRARIES} ${LIBDBUSXX_LIBRARIES})

##
## Install targets
##
install (TARGETS ${SERVICE_NAME} DESTINATION "${INSTALL_BIN_DIR}")