#ifndef NETWORK_SERVICE_WAMP_WRAPPER_HPP
#define NETWORK_SERVICE_WAMP_WRAPPER_HPP

#include <fabui/wamp/backend_service.hpp>
#include "network_service.hpp"

namespace fabui {

class NetworkServiceWrapper: public BackendService {
	public:
		NetworkServiceWrapper(const std::string& env_file, NetworkService& service);

	protected:
		NetworkService& m_service;

		/* backend-service */
		void onConnect();
		void onDisconnect();
		void onJoin();

		/* Service functions */
		void terminate_wrapper(wampcc::invocation_info info);

};

} // namespace fabui

#endif /* NETWORK_SERVICE_WAMP_WRAPPER_HPP */