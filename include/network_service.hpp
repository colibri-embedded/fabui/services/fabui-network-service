#ifndef NETWORK_SERVICE_HPP
#define NETWORK_SERVICE_HPP

#include <fabui/wamp/session.hpp>

namespace fabui {

class NetworkService {
	public:
		/**
		 * Create NetworkService.
		 */
		NetworkService();

		/// Destroy NetworkService
		~NetworkService();

		/**
		 * Set WAMP session
		 */
		void setWampSession(IWampSession* session);

		/**
		 * Get WAMP session
		 */
		IWampSession* getWampSession();

	private:
		/// WAMP session
		IWampSession *m_session;
};

} // namespace fabui

#endif /* NETWORK_SERVICE_HPP */